#include <iostream>
#include <locale>
#include <map>
#include <string>

#ifndef INCLUDED_CIPHER_SHIFTER_H
#include "cipher_shifter.h"
#endif

#ifndef INCLUDED_CIPHER_CHARMAP_H
#include "cipher_charmap.h"
#endif

#ifndef INCLUDED_CIPHER_UTIL_H
#include "cipher_util.h"
#endif

using namespace cipher::shifter;

Shifter::Shifter() {
    d_alphaMap = &cipher::charmap::latinMap;
}

Shifter::Shifter(const std::map<const char, const int> &char_map) {
    d_alphaMap = &char_map;
}

void Shifter::setMap(const std::map<const char, const int> &char_map) {
    d_alphaMap = &char_map;
}

void Shifter::setShift(const int &shift) {
    // handling of negative numbers in mod operations is implementation defined
    // so we will require a local shift
    d_map_size = (int)d_alphaMap->size();
    if (shift < 0) {
        int tmp_shift = shift % d_map_size;
        if (tmp_shift < 0) {
            d_shift = d_map_size + tmp_shift;
        }
    } else {
        d_shift = shift;
    }
}

// shift an entire string character-wise
int Shifter::shiftStr(std::string &input_string) {
    bool shift_failed = false;
    for (char &it : input_string) {
        shift_failed = (bool)Shifter::shiftChar(it);
    }
    if (shift_failed) {
        return cipher::ERRNO::RUNTIME;
    }
    return cipher::ERRNO::SUCCESS;
}

// shift characters in a given map
int Shifter::shiftChar(char &input_char) {
    // if shift is 0 then just return
    if (d_shift == 0) {
        return cipher::ERRNO::SUCCESS;
    }

    std::locale loc;
    char search_char;
    bool input_is_upper = std::isupper(input_char);

    if (input_is_upper) {
        // lower-case the character
        search_char = std::tolower(input_char,loc);
    } else {
        search_char = input_char;
    }

    // search the alphamap for the character
    auto it = d_alphaMap->find(search_char);
    if (it != d_alphaMap->end()) {
        // get the current character value and shift it
        const int output_int = (it->second + d_shift) % d_map_size;
        // try to match it to a value in the map and translate the char
        for (auto it = d_alphaMap->begin(); it != d_alphaMap->end(); ++it) {
            if (it->second == output_int) {
                if (input_is_upper) {
                    input_char = (char)std::toupper(it->first);
                } else {
                    input_char = it->first;
                }
                return cipher::ERRNO::SUCCESS;
            }
        }
        // wasn't able to match the shifted value
        std::cerr << "Couldn't find input '" << search_char << "' shifted by " << d_shift << " in the character map." << std::endl;
        return cipher::ERRNO::RUNTIME;
    }
    // couldn't find the input character so just leave it alone
    return cipher::ERRNO::SUCCESS;
}
