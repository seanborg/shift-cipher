#ifndef INCLUDED_CIPHER_SHIFTER_H
#define INCLUDED_CIPHER_SHIFTER_H

#include <map>
#include <string>

namespace cipher {
namespace shifter {

class Shifter {
public:
    Shifter();
    Shifter(const std::map<const char, const int> &char_map);
    int shiftStr(std::string &input_string);
    void setMap(const std::map<const char, const int> &char_map);
    void setShift(const int &shift);

private:
    int shiftChar(char &input_char);
    const std::map<const char, const int>* d_alphaMap;
    int d_shift, d_map_size;
};

} //
} //
#endif
