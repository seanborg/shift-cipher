#include <iostream>
#include <string>
#include <cstring>
#include <fstream>
#include <vector>

#ifndef INCLUDED_CIPHER_SHIFTER_H
#include "cipher_shifter.h"
#endif

#ifndef INCLUDED_CIPHER_CHARMAP_H
#include "cipher_charmap.h"
#endif

#ifndef INCLUDED_CIPHER_UTIL_H
#include "cipher_util.h"
#endif

void usage(const char *progname) {
    std::cerr << "Usage of " << progname << ":\n";
    std::cerr << "\t--shift=<INTEGER>   integer amount to shift the input\n";
    std::cerr << "\t--input=<STRING>    string to shift\n";
    std::cerr << "\t--file=<PATH>       file to shift\n";
    std::cerr << "\t--charmap={latin}   character map (default is latin characters)\n";
    std::cerr << "\t--output=<PATH>     file to shift\n";
    std::cerr << "\t--verbose           show additional information\n";
    std::cerr << "\t--help              display this message\n";
    std::cerr << "\n\texample: " << progname << "--shift=12 --input=\"this is a string\"\n";
}

const int report_invalid_option(const char *progname, const char* arg) {
    std::cerr << "Invalid option: " << arg << std::endl;
    usage(progname);
    return cipher::ERRNO::OPTION;
}

const int write_output(const char* fname, const std::vector<std::string> outputs, bool verbose) {
    if (fname != nullptr && fname[0] != '\0') {
        if (verbose) {
            std::cout << "Attempting to write result to: " << fname << std::endl;
        }
    } else {
        for (std::string output : outputs) {
            std::cout << output << std::endl;
        }
        return cipher::ERRNO::SUCCESS;
    }

    std::ofstream ofile;
    ofile.open(fname, std::ofstream::out);
    if (!ofile.good()) {
        std::cerr << "Unable to open: " << fname << std::endl;
        return cipher::ERRNO::RUNTIME;
    }

    for (std::string output : outputs) {
        ofile << output << std::endl;
    }

    if (verbose) {
        std::cout << "Result written to: " << fname << std::endl;
    }

    ofile.close();

    if (!ofile.good()) {
        std::cerr << "Unable to close: " << fname << std::endl;
        return cipher::ERRNO::RUNTIME;
    }
    return cipher::ERRNO::SUCCESS;
}

const int read_file(const char *fname, std::vector<std::string> &inputs, bool verbose) {
    if (verbose) {
        std::cout << "Attempting to read input from: " << fname << std::endl;
    }
    if (fname == nullptr || *fname == '\0') {
        std::cerr << "Missing input filename" << std::endl;
        return cipher::ERRNO::OPTION;
    }

    std::ifstream ifile;
    ifile.open(fname, std::ifstream::in);
    if (!ifile.good()) {
        std::cerr << "Unable to open: " << fname << std::endl;
        return cipher::ERRNO::RUNTIME;
    }

    std::string line;
    while(std::getline(ifile, line)) {
        inputs.push_back(line);
    }

    ifile.close();
    if (!ifile.good()) {
        std::cerr << "Unable to close: " << fname << std::endl;
        return cipher::ERRNO::RUNTIME;
    }
    return cipher::ERRNO::SUCCESS;
}


int main(int argc, char *argv[]) {

    std::vector<std::string> inputs;
    char *ofile_n = nullptr, *ifile_n = nullptr, *progname = argv[0];
    int shift;
    bool set_shift = false, verbose = false;

    argv++;
    argc--;
    if (argc == 0) {
        usage(progname);
        return cipher::ERRNO::OPTION;
    }

    // default latinMap shifter
    cipher::shifter::Shifter shifter;

    while (argc > 0) {
        const char *arg = argv[0];
        const char *assign = strchr(arg, '=');
        if (arg[0] != '-' && arg[1] != '-') {
            return report_invalid_option(progname, argv[0]);
        } else {
            arg += 2;
            if (assign) {
                const int keylen = assign - arg;
                const char *value = assign + 1;
                if (strncmp(arg, "shift", keylen) == 0) {
                    const std::string s_max_int = std::to_string(cipher::util::max_int);
                    const std::string s_min_int = std::to_string(cipher::util::min_int);
                    const std::string err_msg = "`--shift=` requires an integer between "
                                                + s_min_int + " and " + s_max_int;
                    try {
                        shift = std::stoi(value);
                        set_shift = true;
                    } catch (std::invalid_argument) {
                        std::cerr << err_msg << std::endl;
                        return cipher::ERRNO::OPTION;
                    } catch (std::out_of_range) {
                        std::cerr << err_msg << std::endl;
                        return cipher::ERRNO::OPTION;
                    }
                } else if (strncmp(arg, "input", keylen) == 0) {
                    inputs.push_back(std::string(value));
                } else if (strncmp(arg, "output", keylen) == 0) {
                    ofile_n = const_cast<char*>(value);
                } else if (strncmp(arg, "file", keylen) == 0) {
                    ifile_n = const_cast<char *>(value);
                } else if (strncmp(arg, "charmap", keylen) == 0) {
                    if (strcmp(value, "latin") == 0) {
                        shifter.setMap(cipher::charmap::latinMap);
                    } else {
                        std::cerr << "Character map " << std::string(value) << " is not available" << std::endl;
                        return cipher::ERRNO::OPTION;
                    }
                } else {
                    return report_invalid_option(progname, argv[0]);
                }
            } else if (strcmp(arg, "help") == 0) {
                usage(progname);
                return cipher::ERRNO::SUCCESS;
            } else if (strcmp(arg, "verbose") == 0) {
                verbose = true;
            } else {
                return report_invalid_option(progname, argv[0]);
            }
        }

        argv++;
        argc--;
    }

    if (ifile_n) {
        if (read_file(ifile_n, inputs, verbose)) {
            return cipher::ERRNO::RUNTIME;
        }
    }

    if (inputs.empty()) {
        std::cerr << "--input or --file options are required" << std::endl;
        return cipher::ERRNO::OPTION;
    }

    if (!set_shift) {
        std::cerr << "--shift option is required" << std::endl;
        return cipher::ERRNO::OPTION;
    }

    shifter.setShift(shift);
    for (std::string &input : inputs) {
        if (verbose) {
            std::cout << "Shifting '" << input << "' by " << shift << std::endl;
        }
        if (shifter.shiftStr(input)) {
            std::cerr << "Failed to correctly shift: " << input << std::endl;
            return cipher::ERRNO::RUNTIME;
        }
    }
    return write_output(ofile_n, inputs, verbose);
}
